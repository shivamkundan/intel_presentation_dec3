\beamer@endinputifotherversion {3.26pt}
\select@language {english}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Scaff-Executor}{5}{0}{2}
\beamer@sectionintoc {3}{Scaff-Scheduler}{9}{0}{3}
\beamer@sectionintoc {4}{Implemented Schedulers}{12}{0}{4}
\beamer@sectionintoc {5}{Using Scaff}{13}{0}{5}
\beamer@sectionintoc {6}{Demo}{15}{0}{6}
