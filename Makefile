BASENAME=main

all:
	export TEXINPUTS
	pdflatex ${BASENAME}.tex
	bibtex ${BASENAME}
	pdflatex ${BASENAME}.tex
	pdflatex ${BASENAME}.tex
	rm -v *.blg *.aux *.log *.bbl

clean:
	if [ -f ${BASENAME}.pdf ]; then (rm -v ${BASENAME}.pdf) fi
	if [ -f *.blg ]; then (rm -v *.blg) fi
	if [ -f *.aux ]; then (rm -v *.aux) fi
	if [ -f *.log ]; then (rm -v *.log) fi
	if [ -f *.bbl ]; then (rm -v *.bbl) fi
